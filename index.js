// Instansiate the global object
qdb = {};
qdb.config = require('./config/config');
qdb.discord = require('discord.js');
qdb.db = require('thinky')(config.db);
qdb.bot = new qdb.discord.Client(config.discordConfig);

var db = require('./config/database'),
    discord = require('./config/discord')
;

console.log("Bootstrapping the database...");
db.bootstrap();

console.log("Logging in...");
discord.loginMyBot();
