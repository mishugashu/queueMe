## 0.1.5 - 2017-09-23

* Converted `!q list` to use Discord's embeded fields.

## 0.1.4 - 2017-04-05

* Added in FC database cleaning for old members.

## 0.1.3 - 2017-03-09

* Added in checking maintenance for the #news channel.

## 0.1.2 - 2017-02-08

## Added

* Refactored lodestone registration to search the FC instead of the whole server.

## 0.1.1 - 2017-02-08

## Added

* `whois` command to see who a person has registered. You need to @mention them

## 0.1.0 - 2016-12-12

## Added

* Synonym for 'clear' - 'cancel'; fundamentally the same command.
* Message to supply the requester with the "complete" command (optional)
  * example: `!q complete 34236ef35 it's in the mail`

## Fixed

* Fixed the `!q help` and `!q list`; apparently he was talking too much in one message.
* Commands that are only ids will stop looking for more information after the id. (uncomplete and clear/cancel)

## 0.0.9 - 2016-09-17

## Added

* Officers can now assign the requester/fulfiller field of a request.

## 0.0.8 - 2016-09-14

## Added

* Broadcasts Lodestone news articles to the #news channel, if there is one.

## 0.0.7 - 2016-09-09

## Changed / Fixed

* Upgraded to discord.js 9, which seemed to have fixed the 401 problems I was starting to get earlier today.
* Started some changes to split my code base to include more functions.

## 0.0.6 - 2016-08-19

## Added

* `!q estimate` will now allow the crafter to put an estimation on a request.

## Changed

* `!q claim` will now accept an estimation after the id, for example `!q claim c5c3f58a7048 5 days` would claim id `c5c3f58a7048` and set the estimation at `5 days`.
* `!q list` will show all of the estimations that are attached to requests
* `!q help` has been updated with the changes

## 0.0.5 - 2016-08-18

## Changed

* How I handle commands within the code - no frontend change
* When `!q list` is not showing everything that `!q list all` would, it lets you know how many are hidden from view.

## 0.0.4 - 2016-08-17

Versioning help features... also yelling.

### Added

* Yell at people who try to edit in commands (thanks @Al)
* Mogzen will now spout out what version he is with `!q version`
* `!q changelog` implemented

## 0.0.3 - 2016-08-11

New features!

### Added

* `!q edit` added to edit requests
* `!q uncomplete` added to... well, uncomplete a request

## 0.0.2 - 2016-07-30

Initial bugfix and attempt at documentation

### Added

* `!q help` will give back some basic implemented functions
* `@bot` mention will get back a !q help
* Bot will only function (besides the help) in a channel called 'requests'
* Anyone who can edit/delete messages ("Officers" in our discord) will be able to perform `!q clear <id>` on anyone's requests. It is limited to the requester, otherwise.
* Uncompleted requests that are "cleared" will be echoed as "cancelled," and otherwise will be echoed as "received."

### Fixed

* `!q clear <id>` will now function properly.

## 0.0.1 - 2016-07-26

Initial release

### Added

* `@role` will queue a message/request
* `!q list` command implemented
* `!q list @role` will filter only requests for that role
* `!q claim <id>` will flag a request with the message author's id, telling others that it is being worked on.
* `!q complete <id>` will flag a request as "complete"
* "complete" will send a PM to the requester that the request is complete
