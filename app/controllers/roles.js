exports.check = function (message,server) {
    var found = [];

    for (var i in message) {
        if (message[i].slice(0,3) === '<@&') {
            var id = message[i].replace(/[<|&|@|>]/g,'');

            if (server.roles.has(id)) found.push(server.roles.get(id));
        }
    }
    return found;
};

exports.roleNames = function (server) {
    if (!server) return null;
    return server.roles.filter(function (a) {
        return a.position >= 0; // We don't care about the everyone role.
    }).map(function (a) {
        return "@" + a.name;
    });
}
