var http = require('http'),
    Entities = require('html-entities').XmlEntities,
    sanitize = require('sanitize-html'),
    lodestoneAPI = 'http://localhost:15151/'
;

let lodestoneNews = (topics,topicType) => {
    qdb.db.seenLodestoneSchema.filter({type:topicType}).then( (alldata) => {
        // If there's absolutely nothing there, we need to not spam the channel.
        var dontAnnounce = false;
        if (alldata.length === 0) { dontAnnounce = true; }

        for (var i = topics.length - 1; i >= 0; i--) {
            var topic = topics[i];

            var topicUrl = topic.url || topic.link;
            if (!topicUrl) continue;
            var topicId = topicUrl.slice(topicUrl.lastIndexOf('/')+1);

            // We've already seen this
            var newTopic;
            var topicTime = new Date(topic.timestamp);
            var oldTopic = alldata.filter(function (d) { return d.lodestoneId === topicId });
            if (oldTopic.length > 0) {
                if (!oldTopic[0].type) { // Upgrade to caching types.
                    newTopic = oldTopic[0];
                    newTopic.type = 'topic';
                }
                else continue;
            } else {
                newTopic = new qdb.db.seenLodestoneSchema({
                    lodestoneId: topicId,
                    date: topicTime,
                    type: topicType
                });
            }

            newTopic.save();

            qdb.bot.guilds.forEach( (guild) => {
                var channel = guild.channels.find('name','ff14news');
                if (!channel) return;

                if (!dontAnnounce && oldTopic.length === 0) {
                    console.log("Announcing new topic: " + topicId + " to " + guild.name + ".");

                    if (topicType === 'topic') {
                        channel.send(topicUrl);
                        channel.send('Kupo, I found this cool new article on Lodestone!\n\r' + Entities.decode(sanitize(topic.html.replace(/\<br.?.?\>/g,'\n\r'),{allowedTags:[],allowedAttributes:[]})));
                    } else if (topicType === 'maintenance' && topic.tag === '[Maintenance]' &&
                        (topic.name.indexOf("All Worlds") >= 0 || topic.name.indexOf("Cactuar") >= 0 || topic.name.indexOf("Aether") >= 0))
                    {
                        const msg = '@here SquareEnix has just announced maintenance for our server, kupo!\n\rMore information found here: ' + topic.link;
                        channel.send(msg);
                    } else {
                        channel.send('I found some interesting news, kupo! ' + topic.link);
                    }
                }
            });
        }
    });
};

exports.checkMaintenance = () => {
    http.get(lodestoneAPI + 'lodestone/maintenance', function (resp) {
        var body = "";
        resp.on('data', (chunk) => {
            body += chunk;
        });
        resp.on('end', () => {
            var topics = JSON.parse(body);

            lodestoneNews(topics,'maintenance')
        });
    });
};
exports.checkMaintenance();

exports.checkTopics = function () {
    http.get(lodestoneAPI + 'lodestone/topics', function (resp) {
        var body = "";
        resp.on('data', (chunk) => {
            body += chunk;
        });
        resp.on('end', () => {
            var topics = JSON.parse(body);

            lodestoneNews(topics,'topic')
        });
    });
}
