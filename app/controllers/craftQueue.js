var general = require('./general'),
    roles = require('./roles');

/**
 * Display the help to the user.
 * @param {object} msg - a discord.js "message" object
 */
exports.displayHelp = function (msg) {
    var message = "Kupo! I am Mogzen. I am here to help you hook up with people who produce things in an organised manner.\n";
    var roleList = [ "@gatherer", "@crafter", "@gardener" ];
    message    += "In order to use my services, please go to the 'requests' channel, and find a role who produces the item you're looking for.\n";
    message    += "Examples of this might be: " + roleList.join(", ") + "\n\r";
    msg.channel.send(message);
    message     = "To interact with me after a request is made, start out your message with '!q' and then give me a command. For example:\n";
    message    += "`!q list` - this will show a list of all unclaimed requests.\n";
    message    += "`!q list all` - this will show all requests, even ones that are claimed and completed.\n";
    message    += "`!q list mine` - this will show all requests you have submitted, or that you have claimed.\n";
    message    += "`!q list @role` - this will show all requests that are submitted for @role.\n";
    message    += "`!q claim <id> <estimation>` - this will allow a producer to claim a request, signifying that they are working on it to other producers. The estimation of completion is optional.\n";
    message    += "`!q complete <id>` - this will allow a producer to signify that they are completed with the request, and is ready to deliver it to the requester.\n";
    message    += "`!q uncomplete <id>` - this will allow a producer to signify that they are not, in fact, completed with the request, despite it being completed earlier.\n";
    message    += "`!q edit <id> <message>` - this will set the request to be whatever is after the id in the message.\n";
    message    += "`!q clear <id>` - this will allow a requester to clear the request off the table, signifying that they have received their order. Without an id, it will clear ALL completed orders that a requester has done.\n";
    message    += "`!q estimate <id> <estimation>` - this will allow someone who has claimed a request to edit or set the estimation of completion of the request.\n";
    message    += "`!q version` will report back version I am currently using.\n";
    message    += "\n\rOfficer only commands:\n";
    message    += "`!q assign <id> <type> <@mention>` - this will assign request 'id' the 'type' to the mentioned person. Types are 'requester' and 'fulfiller'.";
    message    += "\n\rIf I'm malfunctioning, please DM <@132952812198952960>, or email him at mishugashu@gmail.com. My current version is " + qdb.config.version + "!";
    msg.channel.send(message);
};

 /**
  * Display the list of the queue.
  * @param {object} msg - a discord.js "message" object
  * @param {string} message - the raw message, sans the command and the trigger
  */
exports.listQueue = function (msg,message) {
    qdb.db.queueSchema.filter({'server':msg.guild.id}).then(function (data) {
        var filteredData, realCount, retMessages = [];
        data = data.filter(function (a) { return !a.cleared; });
        var originalData = data.length;

        var textToSend;

        if (message.length === 0) {
            textToSend = "Okay, I'll show you all the unclaimed requests...";
            filteredData = data.filter(function (a) { return !a.completed && !a.fulfiller; });
        } else if (message.indexOf("mine") > -1) {
            textToSend = "Okay, I'll show you only your open requests...";
            filteredData = data.filter(function (a) { return (a.requester === msg.author.id || a.fulfiller === msg.author.id); });
        } else {
            textToSend = "Okay, I'll show you all the open requests...";
            filteredData = data;
        }

        var hiddenData = originalData - filteredData.length;
        var rolesArr = roles.check(msg.content.split(' '),msg.guild);

        var requests = [];

        if (rolesArr.length === 0 && filteredData) requests = filteredData;
        else if (rolesArr.length !== 0) {
            textToSend = "Okay, I'll show you all the information for those role(s)...";
            for (var i in filteredData) {
                var found = false;

                for (var z in rolesArr) {
                    if (filteredData[i].mentionIds.indexOf(rolesArr[z].id) > -1) {
                        found = true;
                        break;
                    }
                }

                if (found) requests.push(filteredData[i]);
            }
        }

        var pluralOrNot = hiddenData !== 1 ? "s" : "";
        var areOrIs = hiddenData !== 1 ? "are" : "is";
        var hiddenDataResponse = "But there " + areOrIs + " " + hiddenData.toString() + " request" + pluralOrNot + " not shown in this view. Try 'mine' or 'all' after 'list' to see more.";


        if (requests.length === 0) {
            var response = "The queue is empty!";
            if (hiddenData) response += " " + hiddenDataResponse;
            msg.channel.send(response);
            return;
        }

        for (var i in requests) {
            var r = requests[i],
                mentions = ""
            ;

            for (var x in r.mentionIds) {
                if (mentions !== "") mentions += ", ";
                mentions += "<@&" + r.mentionIds[x] + ">";
            }
            var retObject = {
                name: r.id.slice(r.id.length-12,r.id.length)
            };
            var retMessage = " <@" + r.requester + "> has requested \"" + r.request + "\" from " + mentions + ".";
            if (r.fulfiller) retMessage += " This request has been claimed by <@" + r.fulfiller + ">.";
            if (r.completed) retMessage += " This request has been marked as completed by the fulfiller.";
            if (r.estimation) retMessage += " This request has been estimated for completion as \"" + r.estimation + "\".";
            retObject.value = retMessage;
            retMessages.push(retObject);
        }

        for (var i = 0; i < retMessages.length; i+=25) { // 5 rows per message
            if (i !== 0) { textToSend = "Continuing, kupo...\n\r"; }

            var msgsToSend = retMessages.slice(i,i+25);

            if (i+25 < retMessages.length) textToSend += " I will have more once I catch my breath, kupo.";

            var payload = {embed:{fields:msgsToSend}};

            msg.channel.send(textToSend,payload);

        }
        msg.channel.send(hiddenDataResponse);
    });
};

/**
 * Author of message will claim the request, stating they will accomplish it.
 * @param {object} msg - a discord.js "message" object
 * @param {string} message - the raw message, sans the command and the trigger
 */
exports.claimRequest = function (msg,message) {
    var id = message.split(' ')[0];
    message = message.replace(id,"").trim();

    if (!id) {
        msg.channel.send('Sorry, which id did you want to claim, kupo?\n\rPlease use the proper syntax: `!q claim <id>`');
        return;
    }

    qdb.db.queueSchema.filter(function (row) {
        return row("id").match(id + "$");
    }).then(function (data) {
        data.filter(function (a) { return !a.cleared; });
        if (data.length === 0) {
            msg.channel.send("There are no requests by that id.");
            return;
        }
        data = data[0];
        data.fulfiller = msg.author.id;
        if (message !== "")
            data.estimation = message;

        data.save(function (savedData) {
            var response = "<@" + data.fulfiller + "> has claimed the request from <@" + data.requester + "> for \"" + data.request + "\".";
            if (data.estimation)
                response += " The estimated completion time is \"" + data.estimation + "\".";
            msg.channel.send(response)

        });
    });
};

/**
 * Flag a request as "complete"
 * @param {object} msg - a discord.js "message" object
 * @param {string} message - the raw message, sans the command and the trigger
 */
exports.completeRequest = function (msg, message) {
    var id = message.split(' ')[0];
    message = message.replace(id,"").trim();

    if (!id) {
        msg.channel.send('Sorry, which id did you want to complete, kupo?\n\rPlease use the proper syntax: `!q complete <id>`');
        return;
    }

    qdb.db.queueSchema.filter(function (row) {
        return row("id").match(id + "$");
    }).then(function (data) {
        data.filter(function (a) { return !a.cleared; });
        if (data.length === 0) {
            msg.channel.send("There are no requests by that id.");
            return;
        }
        data = data[0];
        if (data.fulfiller !== msg.author.id) data.fulfiller = msg.author.id;
        data.completed = true;

        data.save(function (savedData) {
            var sendingMessage = "<@" + data.fulfiller + "> has completed your request!";
            if (message.length > 2) sendingMessage += "\n\rThey also sent along this message: " + message;
            msg.guild.members.get(data.requester).send(sendingMessage);
            msg.channel.send("<@" + data.fulfiller + "> has completed the request from <@" + data.requester + "> for \"" + data.request + "\".")
        });
    });
};

/**
 * Unflag a request as "complete"
 * @param {object} msg - a discord.js "message" object
 * @param {string} message - the raw message, sans the command and the trigger
 */
 function uncompleteRequest (msg, message) {
     var id = message.split(' ')[0];

     if (!id) {
         msg.channel.send('Sorry, which id did you want to uncomplete, kupo?\n\rPlease use the proper syntax: `!q uncomplete <id>`');
         return;
     }

     qdb.db.queueSchema.filter(function (row) {
         return row("id").match(id + "$");
     }).then(function (data) {
         data.filter(function (a) { return !a.cleared; });
         if (data.length > 1) {
             msg.channel.send("There's something wrong with this id. Please contact an administrator.");
             console.error("Expected 1 data point, got: ", data);
             return;
         }
         if (data.length === 0) {
             msg.channel.send("There are no requests by that id.");
             return;
         }
         data = data[0];
         if (!data.completed) {
             msg.channel.send("That request has not been completed yet.");
             return;
         }
         data.completed = false;

         data.save(function (savedData) {
             msg.guild.members.get(data.requester).send("<@" + data.fulfiller + "> has reneged their statement that they completed your request!");
             msg.channel.send("<@" + data.fulfiller + "> has marked the request from <@" + data.requester + "> for \"" + data.request + "\" as uncomplete.")
         });
     });
 };

/**
 * Edit a request
 * @param {object} msg - a discord.js "message" object
 * @param {string} message - the raw message, sans the command and the trigger
 */
exports.editRequest = function (msg, message) {
    var id = message.split(' ')[0];
    message = message.replace(id,"").trim();

    if (!id) {
        msg.channel.send('Sorry, which id did you want to edit, kupo?\n\rPlease use the proper syntax: `!q edit <id> <new message>`');
        return;
    }

    qdb.db.queueSchema.filter(function (row) {
        return row("id").match(id + "$");
    }).then(function (data) {
        data.filter(function (a) { return !a.cleared; });
        if (data.length > 1) {
            msg.channel.send("There's something wrong with this id. Please contact an administrator.");
            console.error("Expected 1 data point, got: ", data);
            return;
        }
        if (data.length === 0) {
            msg.channel.send("There are no requests by that id.");
            return;
        }
        data = data[0];
        if (data.completed) {
            msg.channel.send("That has already been completed. No need to edit it.");
            return;
        }

        if (!msg.channel.permissionsFor(msg.author).hasPermission("MANAGE_MESSAGES") && data.requester !== msg.author.id) {
            msg.channel.send("You don't have permission to edit that.");
            return;
        }
        data.request = message;

        data.save(function (savedData) {
            msg.channel.send("<@" + msg.author.id + "> has edited the request from <@" + data.requester + "> to be \"" + data.request + "\".")
        });
    });
};

/**
 * Clear a request, or clear all requests
 * @param {object} msg - a discord.js "message" object
 * @param {string} message - the raw message, sans the command and the trigger. No message means clear all.
 */
exports.clearRequest = function (msg, message) {
    var id = message.split(' ')[0];

    if (message) {
        qdb.db.queueSchema.filter(function (row) {
            return row("id").match(id + "$");
        }).then(function (data) {
            if (data.length > 1) {
                msg.channel.send("There's something wrong with this id. Please contact an administrator.");
                console.error("Expected 1 data point, got: ", data);
                return;
            }
            if (data.length === 0) {
                msg.channel.send("There are no requests by that id.");
                return;
            }
            data = data[0];
            if (!msg.channel.permissionsFor(msg.author).hasPermission("MANAGE_MESSAGES") && data.requester !== msg.author.id) {
                msg.channel.send("You don't have permission to clear that.");
                return;
            }
            data.cleared = true;

            var receivedOrCancelled = "received";
            if (data.completed !== true) receivedOrCancelled = "cancelled";

            qdb.db.queueSchema.save(data,{conflict:'replace'}).then(function (savedData) {
                msg.channel.send("`[" + message.slice(message.length-12,message.length) + "]` The request from <@" + data.requester + "> for \"" + data.request + "\" has been marked as " + receivedOrCancelled + ".");
            });
        });
    } else {
        qdb.db.queueSchema.filter(function (row) {
            return row("requester").eq(msg.author.id);
        }).then(function (data) {
            data = data.filter(function (a) { return a.completed; });
            for (var i in data) {
                data[i].cleared = true;
            }

            qdb.db.queueSchema.save(data,{conflict:'replace'}).then(function (savedData) {
                msg.channel.send("All completed requests from <@" + msg.author.id + "> have been cleared.");
            });
        });
    }
};

/**
 * Estimate (or re-estimate) a request. Can only be done by the person who has claimed it.
 * @param {object} msg - a discord.js "message" object
 * @param {string} message - the raw message, sans the command and the trigger. No message means clear all.
 */
exports.estimateRequest = function (msg, message) {
    var id = message.split(' ')[0];
    message = message.replace(id,"").trim();

    qdb.db.queueSchema.filter(function (row) {
        return row("id").match(id + "$");
    }).then(function (data) {
        data.filter(function (a) { return !a.cleared; });
        if (data.length > 1) {
            msg.channel.send("There's something wrong with this id. Please contact an administrator.");
            console.error("Expected 1 data point, got: ", data);
            return;
        }
        if (data.length === 0) {
            msg.channel.send("There are no requests by that id.");
            return;
        }
        data = data[0];
        if (!data.fulfiller) {
            msg.channel.send("That hasn't even been claimed yet! You can't estimate the completion on this.");
            return;
        }
        if (data.completed) {
            msg.channel.send("That has already been completed. No need to change the estimation on it.");
            return;
        }
        if (!msg.channel.permissionsFor(msg.author).hasPermission("MANAGE_MESSAGES") && data.fulfiller !== msg.author.id) {
            msg.channel.send("You don't have permission to change the estimation on that.");
            return;
        }
        data.estimation = message;

        data.save(function (savedData) {
            msg.channel.send("<@" + msg.author.id + "> has changed the estimation of the request from <@" + data.requester + "> to be \"" + data.estimation + "\".")
        });
    });
};

/**
 * Allow moderators to assign requests to people.
 * @param {object} msg - a discord.js "message" object
 * @param {string} message - the raw message, sans the command and trigger.
 */
exports.assignRequest = function (msg,message) {
    var id = message.split(' ')[0];
    message = message.replace(id,"").trim();

    if (!msg.channel.permissionsFor(msg.author).hasPermission("MANAGE_MESSAGES")) {
        msg.channel.send("You don't have permission to assign requests.");
        return;
    }

    var types = [ 'requester', 'fulfiller' ],
        thisType = message.split(' ')[0];

    if (types.indexOf(thisType) === -1) {
        msg.channel.send("Improperly formatted command, kupo!\nTry `!q assign <id> <type> <@mention>`, like `!q assign 123456789012 fulfiller @mishugashu`.\n\rValid types are: " + types.join(', '));
        return;
    }

    qdb.db.queueSchema.filter(function (row) {
        return row("id").match(id + "$");
    }).then(function (data) {
        data.filter(function (a) { return !a.cleared; });
        if (data.length > 1) {
            msg.channel.send("There's something wrong with this id. Please contact an administrator.");
            console.error("Expected 1 data point, got: ", data);
            return;
        }
        if (data.length === 0) {
            msg.channel.send("There are no requests by that id.");
            return;
        }
        data = data[0];

        var mention = message.match(/<@(\d*?)>/);

        if (!mention || mention.length < 1) {
            msg.channel.send("I couldn't find that person that you're trying to assign this to, kupo!");
            return;
        } else mention = mention[1];

        data[thisType] = mention;

        data.save(function (savedData) {
            msg.channel.send("<@" + msg.author.id + "> has changed the " + thisType + " for the request to be <@" + data[thisType] + ">.")
        });
    });
};
