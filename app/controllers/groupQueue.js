/**
 * Display the help to the user.
 * @param {object} msg - a discord.js "message" object
 */
exports.displayHelp = function (msg) {
    var message = "Kupo! I am Mogzen. I am here to help you hook up with other people who want to do events in an organised manner.\n";
    message    += "In order to use my services, please go to the 'events' channel, and either look for a group or find one!\n\r\n";
    message    += "All commands for group organisation need to start with `!g`! Here's a list of commands I currently support here:\n";
    message    += "`!g list` - this will show a list of all groups that are looking for members.\n";
    message    += "`!g version` will report back version I am currently using.\n"
    message    += "\n\rIf I'm malfunctioning, please DM <@132952812198952960>, or email him at mishugashu@gmail.com. My current version is " + qdb.config.version + "!";
    msg.channel.send(message);
};
