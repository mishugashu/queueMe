var requestsChannel = 'requests',
    eventChannel = 'events',
    groupQueue = require('./groupQueue'),
    craftQueue = require('./craftQueue'),
    lodestone = require('./lodestone')
;

/**
 * We interpret a !g command here.
 * @param {object} msg - a discord.js "message" object
 */
exports.interpretCommand = function (msg) {
    var message = msg.content.slice(3,msg.content.length), // Don't include the command.
        command = msg.content.slice(1,2),
        firstWord = message.split(' ')[0]
    ;

    console.log("[" + new Date() + "] Incoming command (" + msg.content.slice(0,2) + "): <" + msg.author.username + "> is issuing '" + message +"'.");

    var fnObj = exports;
    if (command === 'g') fnObj = groupQueue;
    if (command === 'q') fnObj = craftQueue;

    var viableCommands = {
            // General commands
            'help' : { fn: fnObj.displayHelp, restrict: qdb.triggeringCommands },
            'version' : { fn: exports.displayVersion, restrict: qdb.triggeringCommands },
            'changelog' : { fn: exports.displayChangeLog, restrict: qdb.triggeringCommands },
            'register' : {
                fn: lodestone.registerLodestone,
                restrict: qdb.triggeringCommands
            },
            'whois' : {
                fn: lodestone.whois,
                restrict: qdb.triggeringCommands
            },
            'update' : {
                fn: lodestone.updateRole,
                restrict: qdb.triggeringCommands
            },
            'list' : {
                fn: fnObj.listQueue,
                restrictChannel: requestsChannel,
                restrict: qdb.triggeringCommands
            },

            // Q commands
            'claim' : {
                fn: fnObj.claimRequest,
                restrictChannel: requestsChannel,
                restrict: [ 'q' ]
            },
            'complete' : {
                fn: fnObj.completeRequest,
                restrictChannel: requestsChannel,
                restrict: [ 'q' ]
            },
            'uncomplete' : {
                fn: fnObj.uncompleteRequest,
                restrictChannel: requestsChannel,
                restrict: [ 'q' ]
            },
            'edit' : {
                fn: fnObj.editRequest,
                restrictChannel: requestsChannel,
                restrict: [ 'q' ]
            },
            'clear' : {
                fn: fnObj.clearRequest,
                restrictChannel: requestsChannel,
                restrict: [ 'q' ]
            },
            'estimate' : {
                fn: fnObj.estimateRequest,
                restrictChannel: requestsChannel,
                restrict: [ 'q' ]
            },
            'assign' : {
                fn: fnObj.assignRequest,
                restrictChannel: requestsChannel,
                restrict: ['q']
            }
        }
    ;

    // Synonyms
    viableCommands.cancel = viableCommands.clear;

    if (firstWord in viableCommands) {
        var commandObj = viableCommands[firstWord];

        if (commandObj.restrict && commandObj.restrict.indexOf(command) === -1) {
            msg.channel.send("I don't recognise that command. Try mentioning me, or use the help command.").then( function (message,error) {
                if (error) console.error(error);
                // else console.log(message);
            }) ;
            return;
        }

        // Take out the first word and command from the message.
        message = msg.content.replace(new RegExp("^" + qdb.triggeringSymbol + command + " "),"").replace(new RegExp("^" + firstWord),"").trim();

        if ('restrictChannel' in commandObj && msg.channel.name !== commandObj['restrictChannel']) {
            msg.channel.send( qdb.triggeringSymbol + command + " commands can only be used in the #" + commandObj.restrictChannel + " channel, sorry.");
            return;
        }
        commandObj.fn(msg,message);
        return;
    }

    msg.channel.send("I don't recognise that command. Try mentioning me, or use the help command.");
}

/**
 * Display the change log to the user.
 * @param {object} msg - a discord.js "message" object
 */
exports.displayChangeLog = function (msg) {
    var fs = require('fs');
    fs.readFile( __dirname + '/../../CHANGELOG.md', 'utf8', function (err, data) {
        if (err) {
            msg.channel.send("I couldn't find the changelog!\n\rError has been sent to the administrator.");
            qdb.bot.fetchUser(132952812198952960).then(function (mishu) {
                mishu.send("msg: " + msg + "\n\rserver: " + msg.guild + "\n\rerr: " + err);
            });
            return;
        }

        // Discord Markdown Sucks; trying to compensate.
        data = data.replace(/^\#\#\# (.*?)$/gm,"  ***$1***")
            .replace(/^## (.*?)$/gm,"__***$1***__")
            .replace(/^# (.*?)$/gm,"**$1**")
            .replace(/^\*/gm,"    *")
        ;

        msg.channel.send("Kupo! The change log can get rather long, so I'll send it in a DM.");
        msg.author.send("Here's what I could find...\n\r" + data);
    });
}

/**
 * Display the version of the application.
 * @param {object} msg - a discord.js "message" object
 */
 exports.displayVersion = function (msg) {
     msg.channel.send("I am currently version " + qdb.config.version + "! Thanks for asking! Kupo!");
 }
