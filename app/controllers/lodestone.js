var lodestoneAPI = 'http://localhost:15151/',
    freeCompanyId = '9234490298434913255',
    async = require('async'),
    http = require('http')
;

exports.whois = (msg,message) => {
    var userId = /<@!?(\d*?)>/g.exec(message);

    if (!userId || userId.length < 1) {
        msg.channel.send("I'm sorry, kupo, I couldn't find that discord user.");
        return;
    }
    userId = userId[1];

    qdb.db.userSchema.filter({discordId: userId}).then((characters) =>{
        if (characters.length < 1) {
            msg.channel.send("It appears that person hasn't registered with me, kupo.");
            return;
        }

        // Make sure the character has a Lodestone ID. Otherwise they're not fully registered.
        characters = characters.filter((a) => { return a.lodestoneId; })

        let response = ["Kupo! That person has registered the following names:"];

        for (i in characters) {
            var character = characters[i];
            response.push(character.name + " (" + character.rank + ")");
        }

        msg.channel.send(response.join("\n\r"));
    });
};

exports.refreshData = function (lodestoneId, channel) {
    var filter = {};
    if (lodestoneId) filter = {"lodestoneId":lodestoneId};

    qdb.db.freeCompanySchema.filter({}).then(function (fcArr) {
        qdb.db.userSchema.filter(filter).then(function (users, index) {
            if (users.length === 0) return;
            // console.log("Updating these users: ", users);
            var usersUpdated = 0,
                usersDeleted = []
            ;

            if (users.length > 1 && (lodestoneId || channel)) {
                console.error("weirdness!");
                console.error(lodestoneId);
                console.error(users);
            }
            users.forEach(function (user,index) {
                if (!user) return;

                // Find the user in the FC
                var fcUser = fcArr.find((a) => { return a.name.toLowerCase() === user.name.toLowerCase(); });

                if (!fcUser) {
                    if (channel)
                        channel.send("I can't seem to find a record of " + user.name + " being enrolled in our Free Company. If you just joined, it might take some time for it to show up in our system, and you'll automatically get updated when that happens, kupo!");
                    else {
                        if (Object.keys(user).indexOf("lastUpdated") < 0) {
                            console.error(user.name + " doesn't have a last known date. Adding one now.");
                            user.lastUpdated = new Date();
                        } else {
                            var lastUpdated = new Date (user.lastUpdated);
                            var difference = new Date() - lastUpdated;
                            var purge = false;
                            
                            // if (user.rank != null && user.discordId) {
                            //     var dGuild = qdb.bot.guilds.find('id',user.guildId);
                            //     var dUser = dGuild.members.find('id',user.discordId);
                            //     
                            //     if (!dGuild || !dUser) {
                            //         purge = true; // Discord user or guild isn't here anymore. Might as well get rid of it.
                            //     } else {
                            //         let newRoles = [];
                            //         dUser._roles.forEach((roleId) => {
                            //             if (dGuild.roles.find('name',user.rank) == null) return;
                            //             if (dGuild.roles.find('name',user.rank).id === roleId) return;
                            //             newRoles.push(roleId);
                            //         });
                            //         
                            //         dUser.setRoles(newRoles).catch((err) => {
                            //             console.error("Error trying to change roles of inactive user: ", err);
                            //         }).then((gM) => {
                            //             console.log("Removing old rank from inactive user - " + user.name);
                            //             delete user.rank;
                            //         });
                            //     }
                            // }
                            console.error(user.name + " doesn't seem to be in the FC. Last found in the system, or registered: " + lastUpdated.toLocaleString());
                            if (difference >= 604800000 || purge) { // one week
                                console.error("User " + user.name + " has been in the system (but not the FC) for over a week. Purging.");
                                usersDeleted.push(index);
                                user.delete();
                            }
                        }
                    }
                } else {
                    user.lastUpdated = new Date();

                    // If they previously weren't found and now are, we need to update their user.
                    if (!user.lodestoneId) {
                        user.lodestoneId = ''+fcUser.id;
                        user.rank = '';
                        qdb.bot.guilds.get(user.guildId).members.get(user.discordId).send("Lodestone updated! You are now successfully registered with the Free Company! Thank you, kupo, for being patient.");
                    }

                    // console.log("Comparing " + user.name + " (" + user.rank + ") with " + fcUser.name + " (" + fcUser.rank.name + ").");
                    // check for ranks
                    if (user.discordId && user.guildId && user.lodestoneId) {
                        exports.updateRole(user.discordId,user.guildId,channel,null,'verified',user.name);
                    }

                    if ((!channel && user.rank !== fcUser.rank.name) || channel) {
                        console.log("Rank mismatch or new user found.");
                        var prevRank = user.rank;
                        user.rank = fcUser.rank.name;
                        if (channel)
                            channel.send("Looks like " + user.name + " is a " + user.rank + " in the Free Company, kupo! Let me assign you accordingly...");

                        if (!user.guildId) user.guildId = '146467357022224384';
                        exports.updateRole(user.discordId,user.guildId,channel,prevRank,fcUser.rank.name,user.name);

                    }
                }
                usersUpdated++;

                if (usersUpdated === users.length) { // Last one!
                    // Remove deleted users
                    for (var i = users.length -1; i >= 0; i--) {
                        if (usersDeleted.indexOf(i) >= 0) {
                            users.splice(i, 1);
                        }
                    }

                    qdb.db.userSchema.save(users,{conflict:"replace"}).then(function (us,err) {
                        if (err) {
                            console.error("Updating users got an error: ", err);
                            return;
                        }
                    });
                }
            });
        });
    });
}

exports.updateFreeCompany = (data, refreshData) => {
    qdb.db.freeCompanySchema.filter({}).then(players => {
        // First, remove players that don't exist anymore.
        for (var i = 0; i < players.length; i++) {
            var player = data.filter(a => { return a.id === players[i].id; });

            if (player.length === 0) {
                console.log('Player ' + players[i].name + ' is no longer in FC. Removing from FC database.');
                players[i].delete();
            }
        }

        // Next, update/add players that do exist.
        qdb.db.freeCompanySchema.save(data,{ conflict: "replace" }).then((newPlayers) => {
            console.log("Free Company cache updated.");
            if (refreshData) {
                console.log("Refreshing the User data...");

                // Refresh all User data with the new cache of FC data.
                setTimeout(exports.refreshData,0);
            }
        }, (error) => {
            console.error("There seems to be an error in processing the Free Company cache.\n\rError: ", error);
        });
    });
};

exports.getAllFreeCompany = (callback, page, data, refreshData) => {
    var url = lodestoneAPI + 'freecompany/get/' + freeCompanyId + '/members?page=' + page;

    http.get(url, function (res) {
        var body = "";
        res.on('data', (chunk) => {
            body += chunk;
        });
        res.on('end', () => {
            var ret = JSON.parse(body);

            data = data.concat(ret.results);
            if (ret.paging.total !== ret.paging.end)
                exports.getAllFreeCompany(callback, page+1, data, refreshData)
            else
                callback(data, refreshData);
        });
    });
};

// Update the Free Company cache, which will refresh all data, every hour.
setInterval(exports.getAllFreeCompany,3600000,exports.updateFreeCompany,1,[],true);
setTimeout(exports.getAllFreeCompany,0,exports.updateFreeCompany,1,[],true);

exports.updateRole = (discordId,guildId,channel,prevRank,roleName,username) => {
    qdb.db.userSchema.filter({discordId:discordId,guildId:guildId,name:username}).then((users) => {
        if (users.length < 1 || users.length > 1) return;
        let user = users[0];
        // let roleName = user.rank;

        var dGuild = qdb.bot.guilds.find('id',guildId);
        var dUser = dGuild.members.find('id',discordId);

        if (!dUser) {
            console.error(`Attempting to update role for Discord user that doesn't exist: ${user.name}`);
            return;
        }

        var found = false;
        dUser._roles.forEach((roleId) => {
            if (dGuild.roles.find('id',roleId).name === roleName) found = true;
        });
        if (found) {
            if (channel) {
                channel.send('Looks like ' + user.name + ' already has had their rank assigned in Discord, kupo.');
            // } else {
            //     console.error(`${user.name} is trying to get rank ${roleName}, but already has it.`);
            }
            return;
        }

        let newRole = dGuild.roles.find('name',roleName);
        if (!newRole) {
            if (channel) {
                channel.send('Looks like ' + roleName + ' doesn\'t exist in discord yet, and I\'m not programmed to create it.');
            } else {
                console.error(`${roleName} doesn't exist in discord.`)
            }
            return;
        }

        let newRoles = [];
        dUser._roles.forEach((roleId) => {
            if (prevRank && dGuild.roles.find('name',prevRank).id === roleId) return;
            newRoles.push(roleId);
        });
        newRoles.push(dGuild.roles.find('name',roleName).id);

        dUser.setRoles(newRoles).catch((err) => {
            if (channel) {
                channel.send('Uh-oh, kupo. Looks like I don\'t have permission to alter your rank. Are you someone important? Or perhaps the administrators don\'t like me?');
            } else {
                console.error(`Error setting the role of ${user.name}: ${err}`)
            }
        }).then((gM) => {
            if (channel && roleName !== 'verified') {
                channel.send('I believe that takes care of everything, kupo! Thanks for registering, ' + roleName + ' ' + user.name + '!');
            // } else {
            //     console.log(`Successfully changed ${user.name}'s rank.`);
            }
        });
    });
};

/**
 * Register the user with us.
 * @param {object} msg - a discord.js "message" object
 * @param {string} message - the message content, sans the command and keyword.
 */
exports.registerLodestone = function (msg,message) {
    if (isNaN(parseInt(message))) {
        var world = "Cactuar";
        if (message.split(' ').length >= 3) {
            world = message.split(' ')[2];
            var tmp = mesage.split(' ');
            message = tmp[0] + " " + tmp[1];
        }

        qdb.db.userSchema.filter({name:message}).then(function (users) {
            if (users.length > 0) {
                msg.channel.send("You've already registered your Lodestone profile with me!");
                return;
            }
            msg.channel.send("Okay, kupo!");

            qdb.db.freeCompanySchema.filter((player) => {
                return player("name").match("(?i)^" + message + "$");
            }).then((players) => {
                var character;

                if (players.length < 1) {
                    msg.channel.send("I'm sorry, I didn't find any results, kupo. It's possible that you haven't been in the Free Company long enough to be updated on Lodestone. I'll keep a record of this, and check back with it later.");
                    var user = new qdb.db.userSchema({
                        guildId: ''+msg.guild.id,
                        discordId: msg.author.id,
                        lastUpdated: new Date(),
                        name: message
                    });
                } else if (players.length > 1) {
                    msg.channel.send("That's weird, kupo. I'm finding more than one of you!");
                    return;
                } else {
                    var character = players[0];

                    var user = new qdb.db.userSchema({
                        lodestoneId: ''+character.id,
                        guildId: ''+msg.guild.id,
                        discordId: msg.author.id,
                        lastUpdated: new Date(),
                        name: character.name
                    });
                }

                qdb.db.userSchema.save(user).then(function (user,err2) {
                    if (err2) {
                        msg.channel.send("I'm sorry, something must be wrong with the Lodestone or myself, but I can't process that request.\nError: " + err);
                        return;
                    }

                    if (user.lodestoneId) {
                        msg.channel.send("Okay! I successfully registered " + character.name + "'s Lodestone profile.\n\r" + character.avatar);
                        msg.channel.send("Does that look like you? Let me check against the Free Company and see what rank you are...")
                        exports.refreshData(user.lodestoneId,msg.channel);
                    }
                });
            });
        });
    }
};
