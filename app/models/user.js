var dbType = qdb.db.type;

qdb.db.userSchema = qdb.db.createModel("User", {
    id: dbType.string(),
    name: dbType.string(),
    discordId: dbType.string(),
    lodestoneId: dbType.string(),
    rank: dbType.string(),
    lastUpdated: dbType.date()
});

// Uncomment if we want to do a lodestone refresh on boot.
// setTimeout(require('../controllers/lodestone').refreshData,5000);
