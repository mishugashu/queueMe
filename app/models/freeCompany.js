var dbType = qdb.db.type;

// This model is created by lodestone-nodejs
// https://github.com/viion/lodestone-nodejs
qdb.db.freeCompanySchema = qdb.db.createModel("FreeCompany", {
    id: dbType.number(),
    avatar: dbType.string(),
    name: dbType.string(),
    server: dbType.string(),
    rank: {
        icon: dbType.string(),
        name: dbType.string()
    },
    class: {
        icon: dbType.string(),
        level: dbType.number()
    },
    grand_company: {
        icon: dbType.string(),
        name: dbType.string(),
        rank: dbType.string()
    }
});

// Do a Lodestone FC update on boot if it's empty.
// qdb.db.freeCompanySchema.filter({}).then((characters) => {
//     if (characters.length === 0) {
//         var lodestone = require('../controllers/lodestone');
//         lodestone.getAllFreeCompany(lodestone.updateFreeCompany,1,[],true);
//     }
// });
