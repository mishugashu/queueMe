var dbType = qdb.db.type;

qdb.db.queueSchema = qdb.db.createModel("Queue", {
    id: dbType.string(),
    mentionIds: dbType.array(),
    request: dbType.string(),
    requester: dbType.string(),
    fulfiller: dbType.string(),
    completed: dbType.boolean(),
    server: dbType.string(),
    cleared: dbType.boolean(),
    estimation: dbType.string()
});
