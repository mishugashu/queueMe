var dbType = qdb.db.type;

qdb.db.seenLodestoneSchema = qdb.db.createModel("SeenLodestone", {
    id: dbType.string(),
    lodestoneId: dbType.string(),
    date: dbType.date(),
    type: dbType.string()
});

var lodestoneTopics = require('../controllers/lodestoneTopics');

setInterval(lodestoneTopics.checkTopics,180000); // We want to do this five minutes.
// lodestoneTopics.checkTopics(); // And on startup.

setInterval(lodestoneTopics.checkMaintenance,360000); // We want to do this ten minutes.
lodestoneTopics.checkMaintenance(); // And on startup.
