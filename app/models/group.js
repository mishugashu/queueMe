var dbType = qdb.db.type;

qdb.db.groupSchema = qdb.db.createModel("Group", {
    id: dbType.string(),
    eventName: dbType.string(),
    eventLevel: dbType.number(),
    requester: dbType.string(),
    rolesNeeded: {
        "tank": dbType.number(),
        "healer": dbType.number(),
        "damage": dbType.number()
    },
    responders: [ dbType.string() ]
});
