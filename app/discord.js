var roles = require('./controllers/roles'),
    general = require('./controllers/general')
;

qdb.triggeringCommands = [
    'q'
    // 'g'
 ];
qdb.triggeringSymbol = '!';

exports.newMember = (member) => {
    const channel = member.guild.channels.find('name', 'general');
    const botqueries = member.guild.channels.find('name', 'botqueries');
    if (!channel) return;
    channel.send(`Welcome to ${member.guild.name}, ${member}. Please go to <#${botqueries.id}> and register your FFXIV character by using \`!q register Firstname Lastname\`. Thank you!`);
};

/**
 * We get an error message from discord.
 */
exports.error = function (msg) {
    console.error("ERROR: ", msg);
};

/**
 * We get a warning message from discord.
 */
exports.warn = function (msg) {
    console.warn("WARN: ", msg);
};

/**
 * What we need to do after we successfully log in and are ready.
 */
exports.ready = function () {
    console.log(qdb.bot.user.username + " is ready! ");
    qdb.bot.user.setStatus("online");
    qdb.bot.user.setGame("Seek and Destroy");
};

/**
 * What to do when we lose connection to discord.
 */
exports.disconnected = function () {
    console.error("Client was disconnected.");
    setTimeout(function () {
        // Relog if disconnected
        console.log("Attempting to log in...");
        exports.loginMyBot();
    });
};

/**
 * Snarky response to someone doing an edit on a command.
 * @param {object} msg - a discord.js "message" object
 */
exports.onEdit = function (oldMsg,msg) {
    var roleNames = roles.roleNames(msg.guild),
        mentioned = roles.check(roleNames,msg.guild)
    ;

    if (qdb.triggeringCommands.indexOf(msg.content.slice(1,2)) !== -1 && msg.content.slice(0,1) === qdb.triggeringSymbol) {
        msg.channel.send("Kupo! You're doing it wrong, <@" + msg.author.id + ">! I don't respond to edited messages!");
        return;
    }
};

/**
 * We receive a message.
 * @param {object} msg - a discord.js "message" object
 */
exports.message = function (msg) {
    // We don't care about our own messages,
    if (msg.author.id === qdb.bot.user.id) return;
    // console.log(msg.content);

    // Pass this message to the interpret command function of the proper file.
    if (msg.content.slice(0,1) === qdb.triggeringSymbol && qdb.triggeringCommands.indexOf(msg.content.slice(1,2)) !== -1) { // Our triggering command
        general.interpretCommand(msg);
        return;
    }

    var roleNames = roles.roleNames(msg.guild),
        mentioned = roles.check(msg.content.split(' '),msg.guild),
        match = false
    ;

    if (mentioned.length > 0 && msg.channel.name === "requests") {
        var mentionIds = [],
            mentions = msg.content.match(new RegExp('<@&(.*?)>','g')),
            request = msg.content.replace(new RegExp('[ ]?<@&.*?>[ ]?','g'),' ').trim()
        ;

        for (var i in mentions) {
            mentionIds.push(mentions[i].replace(/[^\d]/g,''));
        }

        if (mentions && request) {
            var queue = new qdb.db.queueSchema({
                mentionIds : mentionIds,
                request : request,
                requester : msg.author.id,
                server: msg.guild.id
            });

            queue.save(function (error,data) {
                var retMessage = "`[" + queue.id.slice(queue.id.length-12,queue.id.length) + "]`";
                retMessage += " Creating request for " + mentions.join(", ") + " from <@" + queue.requester + ">. This request reads:\n\r" + request;
                msg.channel.send(retMessage);
            });
            return;
        }

    }

    if (msg.content.match(/s+\s?t+\s?o+\s?r+\s?m+\s?b+\s?[li]+\s?o+\s?o+\s?d+/i)) match = true;
    if (msg.content.match(/(\s|sb|^|\.|,|~)s(\.|,|~|\?|\s)*?b(\s|\?|\.|$|,|~)/i)) match = true;
    if (msg.content.match(/4\.[0xX]/i)) match = true;
    if (msg.content.match(/the (new )?expansion/i)) match = true;
    if (msg.content.match(/(\s|^|\.|,|~)+(queue|que|90k|server)(\s|^|\.|,|~)+/i)) match = true;

    // if (match) {
    //     var sb = new Date("June 16 2017 04:00:00");
    //     var difference = sb - new Date();
    //     var days = Math.floor((((difference / 1000) / 60) / 60) / 24);
    //     var hours = Math.floor(((difference/1000)/60)/60) - (days*24);
    //     var minutes = Math.floor((difference/1000)/60) - ((hours+days*24)*60);
    //     var seconds = Math.floor(difference/1000) % 60;
    //
    //     var retMsg = "Did someone say Stormblood, kupo?!\nOnly ";
    //     if (days >= 0 && hours >= 0 && minutes >= 0) {
    //         var daysStr = (days != 1 ? "s" : "");
    //         var hoursStr = (hours != 1 ? "s" : "");
    //         var minutesStr = (minutes != 1 ? "s" : "");
    //         var secondsStr = (seconds != 1 ? "s" : "");
    //         if (days > 0)
    //             retMsg += days + " more day" + daysStr + ", " + hours + " more hour" + hoursStr + ", " + minutes + " more minute" + minutesStr + ", and " + seconds + " more second" + secondsStr + " until Stormblood Early Access!";
    //         else if (hours > 0)
    //             retMsg += hours + " more hour" + hoursStr + ", " + minutes + " more minute" + minutesStr + ", and " + seconds + " more second" + secondsStr + " until Stormblood Early Access!";
    //         else
    //             retMsg += minutes + " more minute" + minutesStr + ", and " + seconds + " more second" + secondsStr + " until Stormblood Early Access!";
    //     } else { return; } // SB RELEASE IS OVER
    //     if (msg.channel.name !== "botqueries") retMsg += "\nIf you are querying me specifically for this information, please use #botqueries."
    //     msg.channel.send(retMsg);
    // }

    if (msg.channel.name === "general" && match) {
        let em = msg.guild.emojis.find("name","mog")

        if (em)
            msg.react(em).then(() => {}).catch(err => { console.log("ERR:",err);});
    }

    // If we're being mentioned.
    if (msg.mentions.users.find('id',qdb.bot.user.id)) {
        var message = "Kupo! I am Mogzen. I have multiple functions. I like to organise things for you!\n";
        message    += "\n\rI have two major functions right now.\n";
        message    += "My first function is to handle crafting requests and put them in a queue for crafters and gatherers!\n";
        message    += "To learn more about this, please go to the #requests channel and type `!q help`!\n";
        // message    += "\n\rMy second function is to get people together to tackle things as a team. This can be anything, like Alexander's Fist of the Father, unsynced Thornmarch, or any other number of things.\n";
        // message    += "To learn more about this, please go to the #events channel and type `!g help`!\n"
        message    += "\n\rIf I'm malfunctioning, please DM <@132952812198952960>, or email him at mishugashu@gmail.com. My current version is " + qdb.config.version + "!";
        msg.channel.send(message);
    }
};

/**
 * Log the bot into discord.
 */
exports.loginMyBot = function () {
    qdb.bot.login(config.token).then( function (token, err) {
        if (err) {
            console.error(err);
            setTimeout(function () {
                process.exit(1);
            }, 2000);
        }
        if (!token) {
            console.warn("failed to connect");
            setTimeout(function () {
                process.exit(0);
            }, 2000);
        }
    });
};
