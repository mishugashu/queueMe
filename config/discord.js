var discord = require('../app/discord');

qdb.bot.on("error", discord.error);
qdb.bot.on("warn", discord.warn);
qdb.bot.on("ready", discord.ready);
qdb.bot.on("disconnected", discord.disconnected);
qdb.bot.on("message", discord.message);
qdb.bot.on("messageUpdated", discord.onEdit);
qdb.bot.on('guildMemberAdd', discord.newMember);

exports.loginMyBot = discord.loginMyBot;
