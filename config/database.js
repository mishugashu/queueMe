var fs = require('fs'),
    config = qdb.config
;

exports.bootstrap = function () {
    // Bootstrap ORM models
    var models_path = __dirname + '/../app/models';
    var files = fs.readdirSync(models_path);
    files.reverse().forEach(function (file) {
        if (file.indexOf('.js') > -1)
        require(models_path + '/' + file);
    });
};
