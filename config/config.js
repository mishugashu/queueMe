var configFile = 'config.json';

if (process.env.NODE_ENV === 'development') {
    console.log("Development NODE_ENV, loading development config file.");
    configFile = 'config-dev.json';
}

try {
    config = require('./' + configFile);
} catch (e) {
    console.error("Cannot read config file in ./config/" + configFile);
    process.exit(1);
}

if (!config.token) {
    console.error("Token not found in ./config/" + configFile);
    process.exit(1);
}

if (!config.discordConfig)
    config.discordConfig = {};

if (!config.db.host)    config.host = 'localhost';
if (!config.db.port)    config.port = 28015;
if (!config.db.db)      config.db   = 'queueme';

config.version = require('../package.json')['version'];

console.log("Database configuration: ", config.db)

module.exports = config;
